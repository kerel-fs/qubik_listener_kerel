#!/usr/bin/env python3
""" QUBIK frame forwarder from flowgraph to satnogs db """

import argparse
import binascii
import requests
import socket

from datetime import datetime, timezone
from pprint import pprint

from satnogsdecoders import __version__ as decoders_version
from satnogsdecoders.decode_frame import decode_frame


def sids_submit_frame(args, norad_id, timestamp, data):
    params = {
        'noradID': norad_id,
        'source': args.callsign,
        'timestamp': datetime.strftime(timestamp,'%Y-%m-%dT%H:%M:%S.000Z'),
        'locator': 'longLat',
        'longitude': args.lon,
        'latitude': args.lat,
        'frame': data.hex().replace(' ', ''),
    }

    try:
        response = requests.post(args.telemetry_url, data=params, timeout=120)
        response.raise_for_status()
    except requests.HTTPError as e:
        print("ERROR: Frame forwarding failed, status code: {}".format(response.status_code))


def packet_listener_and_forwarder(args):
    sock = socket.socket(socket.AF_INET,    # Internet
                         socket.SOCK_DGRAM) # UDP
    sock.bind((args.udp_ip, args.udp_port))

    print ("Listening on {}:{}".format(args.udp_ip, args.udp_port))
    
    while True:
        # Listen for frame
        data, addr = sock.recvfrom(200)

        # Generate timestamp
        timestamp = datetime.now(timezone.utc)

        print("Received frame,")
        try:
            print("\tASCII: {}".format(data.decode('ascii')))
        except UnicodeDecodeError:
            # TODO: Ignore non-unicode characters
            pass

        print("\tHEX: {}".format(binascii.hexlify(data).decode('ascii')))

        frame = decode_frame('qubik', data)

        pprint(frame)

        if frame['spacecraft_id'] == 1:
            norad_id = args.norad_id_qubik1

            # Send frame to satnogs-db
            sids_submit_frame(args, norad_id, timestamp, data)
        elif frame['spacecraft_id'] == 2:
            norad_id = args.norad_id_qubik2

            # Send frame to satnogs-db
            sids_submit_frame(args, norad_id, timestamp, data)
        else:
            print("ERROR: Unexpected spacecraft id, {}.".format(frame['spacecraft_id']))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Listen for frames via UDP and forward them to satnogs-db.')
    parser.add_argument('--norad_id_qubik1', type=int,
                        default=99500,
                        help='NORAD ID, e.g. 99500')
    parser.add_argument('--norad_id_qubik2', type=int,
                        default=99501,
                        help='NORAD ID, e.g. 99501')
    parser.add_argument('--callsign', type=str,
                        default="ZZ0ZZZ",
                        help='Callsign, e.g. ZZ0ZZZ')
    parser.add_argument('--lat', type=str,
                        default="23.50000E",
                        help='Latitude, e.g. 23.50000E')
    parser.add_argument('--lon', type=str,
                        default="53.25000N",
                        help='Longitude, e.g. 53.25000N')
    parser.add_argument('--udp_ip', type=str,
                        default="127.0.0.1",
                        help='GRC UDP IP, e.g. 127.0.0.1')
    parser.add_argument('--udp_port', type=int,
                        default=16982,
                        help='GRC UDP Port, e.g. 16982')
    parser.add_argument('--telemetry_url', type=str,
                        default="https://db-dev.satnogs.org/api/telemetry/",
                        help='SatNOGS DB API Telementry endpoint, e.g. https://db-dev.satnogs.org/api/telemetry/')

    args = parser.parse_args()

    try:
        packet_listener_and_forwarder(args)
    except KeyboardInterrupt:
        print("Aborted.")
